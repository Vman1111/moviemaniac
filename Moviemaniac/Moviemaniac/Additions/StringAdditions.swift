//
//  StringAdditions.swift
//  Moviemaniac
//
//  Created by Vytautas Sapranavicius on 29/07/2021.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: self)
    }
}
