//
//  ViewControllerAdditions.swift
//  Moviemaniac
//
//  Created by Vytautas Sapranavicius on 27/07/2021.
//

import UIKit

extension UIViewController: RequestDelegate {
    
    func onError() {
        DispatchQueue.main.async() {
            let alert = UIAlertController(title: "error.popup.title".localized,
                                          message: "error.popup.message".localized,
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "error.popup.dismiss".localized, style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
}
