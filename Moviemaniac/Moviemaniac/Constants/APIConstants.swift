//
//  APIConstants.swift
//  Moviemaniac
//
//  Created by Vytautas Sapranavicius on 26/07/2021.
//

import Foundation

struct APIConstants {
    static let apiKeyString = "?api_key=a874a785f5b47354475d14574a969a7a"
    
    //MARK: - Base API URL
    static let base = "https://api.themoviedb.org/3"
    
    //MARK: - List URLs
    static let popularMovies = "/movie/popular"
    static let topRated = "/movie/top_rated"
    static let configuration = "/configuration"
}
