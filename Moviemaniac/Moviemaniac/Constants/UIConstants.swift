//
//  Constants.swift
//  Moviemaniac
//
//  Created by Vytautas Sapranavicius on 27/07/2021.
//

import UIKit


struct UIConstants {
    
    static let cellWidth = UIScreen.main.bounds.width - 32
    static let cellheight = UIScreen.main.bounds.height / 4.17
    
}
