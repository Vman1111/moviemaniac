//
//  APIRequests.swift
//  Moviemaniac
//
//  Created by Vytautas Sapranavicius on 27/07/2021.
//

import Foundation

protocol  RequestDelegate: AnyObject {
    func onError()
}

public struct Request<Model: Codable> {
    
    public typealias SuccessCompletionHandeler = (_ response: Model) -> Void
    
    static func get(_ delegate: RequestDelegate?,
                    path: String,
                    url: String,
                    success successCallback: @escaping SuccessCompletionHandeler) {
        
        guard let urlComponent = URLComponents(string: url), let usableUrl = urlComponent.url else {
            delegate?.onError()
            return
        }
        
        var request = URLRequest(url: usableUrl)
        request.httpMethod = "GET"
        
        var dataTask: URLSessionDataTask?
        let defaultSession = URLSession(configuration: .default)
        
        dataTask = defaultSession.dataTask(with: request) { data, response, error in
            defer {
                dataTask = nil
            }
            if error != nil {
                delegate?.onError()
            } else if let data = data,
                      let response = response as? HTTPURLResponse,
                      response.statusCode == 200 {
                print("URL: \(url)")
                print("\nResponse Data: \(data)")
                print("\nStatus code: \(response.statusCode)\n")
                guard let model = self.parseModel(with: data, at: path)
                else {
                    delegate?.onError()
                    return
                }
                successCallback(model)
            }
        }
    dataTask?.resume()
    }
    
    static func parseModel(with data: Data, at path: String) -> Model? {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary
            
            if let dictAtPath = json?.value(forKey: path) {
                let jsonData = try JSONSerialization.data(withJSONObject: dictAtPath, options: .prettyPrinted)
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let model = try decoder.decode(Model.self, from: jsonData)
                return model
            } else {
                return nil
            }
        } catch {
            return nil
        }
    }
    
}
    

