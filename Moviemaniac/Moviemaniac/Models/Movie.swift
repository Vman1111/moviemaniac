//
//  Movie.swift
//  Moviemaniac
//
//  Created by Vytautas Sapranavicius on 27/07/2021.
//

import Foundation

struct Movie: Codable {
    
    var id: Int?
    var title: String?
    var popularity: Double?
    var backdropPath: String?
    var genreIds: [Int]?
    var releaseDate: String?
    var overview: String?
    
}

