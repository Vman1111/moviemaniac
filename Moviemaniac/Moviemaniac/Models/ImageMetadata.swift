//
//  ImageMetadata.swift
//  Moviemaniac
//
//  Created by Vytautas Sapranavicius on 27/07/2021.
//

import Foundation

struct ImageMetadata: Codable {
    
    var baseUrl: String?
    var secureBaseUrl: String?
    var backdropSizes: [String]?
    var logoSizes: [String]?
    var posterSizes: [String]?
    var profileSizes: [String]?
    var stillSizes: [String]?
    
}
