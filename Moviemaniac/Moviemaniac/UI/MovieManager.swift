//
//  MovieManager.swift
//  Moviemaniac
//
//  Created by Vytautas Sapranavicius on 27/07/2021.
//

import Foundation

class MovieManager {
    
    var imageMetadata: ImageMetadata?
    
    init() {
        getImageConfiguration(delegate: self)
    }
    
    public func getImageConfiguration(delegate: RequestDelegate) {
        let url = "\(APIConstants.base)\(APIConstants.configuration)\(APIConstants.apiKeyString)"
        Request<ImageMetadata>.get(delegate, path: "images", url: url) { (imageMetaData) in
            self.imageMetadata = imageMetaData
        }
    }
    
}

extension MovieManager: RequestDelegate {
    
    func onError() {
        //TODO: - handle error if not receiving image metadata
    }
    
}
