//
//  SortOrderView.swift
//  Moviemaniac
//
//  Created by Vytautas Sapranavicius on 28/07/2021.
//

import UIKit

protocol MovieUpdaterDelegate {
    func topRatedClicked()
    func popularClicked()
}

class SortOrderView: UIView {
    
    var delegate: MovieUpdaterDelegate?
    var blurEffectView: UIVisualEffectView?
    let closeButton = UIButton()
    let stackView = UIStackView()
    let topRatedButton = UIButton()
    let popularButton = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    convenience init(frame: CGRect, delegate: MovieUpdaterDelegate) {
        self.init(frame: frame)
        self.delegate = delegate
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        self.backgroundColor = UIColor.clear
        addBlurEffect()
        setupCloseButton()
        setupStackView()
    }
    
    private func addBlurEffect() {
        let blurEffect = UIBlurEffect(style: .light)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        guard let blurEffectView = blurEffectView else { return }
        blurEffectView.frame = self.frame
        self.addSubview(blurEffectView)
    }
    
    private func setupCloseButton() {
        self.addSubview(closeButton)
        closeButton.setImage(UIImage(systemName: "x.circle", withConfiguration: UIImage.SymbolConfiguration(weight: .bold)), for: .normal)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        closeButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 64).isActive = true
        closeButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -32).isActive = true
        closeButton.addTarget(self, action: #selector(dismiss), for: .touchUpInside)
        closeButton.tintColor = UIColor.black
        self.bringSubviewToFront(closeButton)
    }
    
    @objc private func dismiss() {
        self.isHidden = true
    }
    
    private func setupStackView() {
        self.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.widthAnchor.constraint(equalToConstant: self.bounds.width).isActive = true
        stackView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        stackView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .fill
    
        setupOptioButtons()
        stackView.addArrangedSubview(topRatedButton)
        stackView.addArrangedSubview(popularButton)
    }
    
    private func setupOptioButtons() {
        topRatedButton.setTitle("sort.order.top.rated".localized, for: .normal)
        topRatedButton.setTitleColor(UIColor.black, for: .normal)
        topRatedButton.addTarget(self, action: #selector(getTopRatedMovies), for: .touchUpInside)
        
        popularButton.setTitle("sort.order.popular".localized, for: .normal)
        popularButton.addTarget(self, action: #selector(getPopularMovies), for: .touchUpInside)
        popularButton.setTitleColor(UIColor.black, for: .normal)
    }
    
    @objc private func getTopRatedMovies() {
        delegate?.topRatedClicked()
        self.isHidden = true
    }
    
    @objc private func getPopularMovies() {
        delegate?.popularClicked()
        self.isHidden = true
    }
    
}



