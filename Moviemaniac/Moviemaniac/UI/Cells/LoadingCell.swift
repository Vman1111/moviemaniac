//
//  LoadingCell.swift
//  Moviemaniac
//
//  Created by Vytautas Sapranavicius on 27/07/2021.
//

import UIKit

class LoadingCell: UICollectionViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
