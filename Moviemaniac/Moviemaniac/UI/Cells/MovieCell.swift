//
//  MovieCell.swift
//  Moviemaniac
//
//  Created by Vytautas Sapranavicius on 27/07/2021.
//

import UIKit

class MovieCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var releaseDate: UILabel!
    @IBOutlet weak var popularity: UILabel!
    @IBOutlet weak var loadingSpinner: UIActivityIndicatorView!
    
    var background: UIView?
    private var imageMetadata: ImageMetadata?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundView = UIView(frame: self.frame)
        if let background = background {
            background.backgroundColor = UIColor.systemGray
        }
        self.imageView.layer.cornerRadius = 5
        loadingSpinner.isHidden = true
        self.backgroundView = background
        title.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        releaseDate.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        popularity.backgroundColor = UIColor.white.withAlphaComponent(0.5)
    }
    
    private func startSpinner() {
        loadingSpinner.startAnimating()
        loadingSpinner.isHidden = false
    }
    
    private func stopSpinner() {
        loadingSpinner.stopAnimating()
        loadingSpinner.isHidden = true
    }
    
    public func configure(with movie: Movie, manager: MovieManager) {
        if let title = movie.title, let releaseDate = movie.releaseDate, let popularity = movie.popularity {
            self.title.text = "\(title)"
            self.title.frame = CGRect(x: 0.0,
                                 y: 0.0,
                                 width: title.width(withConstrainedHeight: 26,
                                                    font: UIFont.systemFont(ofSize: 16)) + 10,
                                 height: 30)
            
            self.releaseDate.text = String(format: "movie.release.date".localized, releaseDate)
            self.releaseDate.frame = CGRect(x: 0.0,
                                 y: 0.0,
                                 width: releaseDate.width(withConstrainedHeight: 26,
                                                    font: UIFont.systemFont(ofSize: 16)) + 10,
                                 height: 30)
            let popularityText: String = "\(Int(popularity))"
            self.popularity.text = String(format: "movie.popularity".localized, popularityText)
            self.popularity.frame = CGRect(x: 0.0,
                                 y: 0.0,
                                 width: popularityText.width(withConstrainedHeight: 26,
                                                    font: UIFont.systemFont(ofSize: 16)) + 10,
                                 height: 30)
            setImage(manager, movie)
            self.layer.cornerRadius = 5
        }
    }
    
    private func setImage(_ manager: MovieManager, _ movie: Movie) {
        startSpinner()
        guard let urlString = createUrl(manager: manager, movie: movie) else { return }
            
            let session = URLSession.shared
            let url = URL(string: urlString)
            
            let dataTask = session.dataTask(with: url!) { (data, response, error) in
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else { return }
                    self.stopSpinner()
                    if let data = data {
                        self.imageView.image = UIImage(data: data)
                        self.background?.addSubview(self.imageView)
                    } else {
                        self.imageView.image = UIImage(named: "error")
                    }
                }
            }
            dataTask.resume()
    }
    
    private func createUrl(manager: MovieManager, movie: Movie) -> String? {
        var urlString: String?
        if let imageMetadata = manager.imageMetadata, let size = imageMetadata.backdropSizes {
            guard let baseUrl = imageMetadata.secureBaseUrl else { return nil }
            guard let imagePath = movie.backdropPath else { return nil }
            let imageSize = size[1]
            urlString = "\(baseUrl)\(imageSize)\(imagePath)"
        }
        return urlString
    }

}

extension MovieCell: RequestDelegate {
    func onError() {
        DispatchQueue.main.async {
            self.imageView.image = UIImage(named: "error")
        }
    }
}
extension String {
    func width(withConstrainedHeight heigh: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: heigh)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font : font], context: nil)
        return ceil(boundingBox.width)
    }
}

