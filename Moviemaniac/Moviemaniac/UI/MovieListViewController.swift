//
//  MovieListViewController.swift
//  Moviemaniac
//
//  Created by Vytautas Sapranavicius on 27/07/2021.
//

import UIKit

class MovieListViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var sortOrderButton: UIButton!
    
    let totalPages = 500
    let manager = MovieManager()
    var movies: [Movie] = []
    var currentPage: Int = 1
    var currentPath: String = APIConstants.popularMovies
    
    private let loadingCellId = "loading.cell.id"
    private let movieCellId = "movie.cell.id"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = "MovieManiac"
        sortOrderButton.setTitle("sort.order.title".localized, for: .normal)
        
        getMovies(page: currentPage, delegate: self)
        setupCollectionView()
        setupSortOrderButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        collectionView.reloadData()
    }
    
    public func reloadCollectionView() {
        self.collectionView.reloadData()
    }
    
    private func setupSortOrderButton() {
        sortOrderButton.addTarget(self, action: #selector(showSortOrderView), for: .touchUpInside)
    }
    
    @objc private func showSortOrderView() {
        let view = SortOrderView(frame: self.view.frame, delegate: self)
        self.view.addSubview(view)
        
    }
    
    private func addBlurEffect() {
        
    }
    
    private func getMovies(page: Int, delegate: RequestDelegate) {
        let url = "\(APIConstants.base)\(currentPath)\(APIConstants.apiKeyString)&page=\(page)"
        Request<[Movie]>.get(delegate, path: "results", url: url) { (movies) in
            for movie in movies {
                self.movies.append(movie)
            }
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "LoadingCell", bundle: self.nibBundle), forCellWithReuseIdentifier: loadingCellId)
        collectionView.register(UINib(nibName: "MovieCell", bundle: self.nibBundle), forCellWithReuseIdentifier: movieCellId)
        collectionView.layer.cornerRadius = 5
    }


}

//MARK: - CollectionView Delegate Methods
extension MovieListViewController: UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    
}

//MARK: - CollectionView DataSource Methods
extension MovieListViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if currentPage < totalPages && indexPath.row == movies.count - 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: loadingCellId, for: indexPath) as! LoadingCell
            cell.activityIndicator.startAnimating()
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: movieCellId, for: indexPath) as! MovieCell
            cell.configure(with: movies[indexPath.row], manager: manager)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if currentPage < totalPages && indexPath.row == movies.count - 1 {
            currentPage += 1
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self.getMovies(page: self.currentPage, delegate: self)
            }
        }
    }
    
}

//MARK: - CollectionView DelegateFlowLayout methods
extension MovieListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: UIConstants.cellWidth, height: UIConstants.cellheight)
        return size
    }
    
}

//MARK: - MovieUpdaterDelegate Methods
extension MovieListViewController: MovieUpdaterDelegate {
    func topRatedClicked() {
        collectionView.reloadData()
        self.currentPath = APIConstants.topRated
        self.currentPage = 1
        self.movies = []
        self.getMovies(page: currentPage, delegate: self)
    }
    
    func popularClicked() {
        collectionView.reloadData()
        self.currentPath = APIConstants.popularMovies
        self.currentPage = 1
        self.movies = []
        self.getMovies(page: currentPage, delegate: self)
    }
}
